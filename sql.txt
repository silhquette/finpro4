CREATE DATABASE finproweb;

CREATE TABLE `users` (
 usersID int NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  username varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  bio varchar(255),
  profile_photo varchar(255),
  PRIMARY KEY(usersID)
);

CREATE TABLE `finproweb`.`post` ( 
`postID` INT(11) NOT NULL AUTO_INCREMENT ,  
`userID` INT(11) NOT NULL ,  
`photo` VARCHAR(100) NOT NULL ,  
`caption` LONGTEXT NULL ,  
`date` DATE NOT NULL ,    
PRIMARY KEY  (`postID`)) 
ENGINE = InnoDB;

CREATE TABLE `comment`(
  commentID int NOT NULL AUTO_INCREMENT,
  commentUser varchar(255),
  postID int,
  content varchar(255),
  PRIMARY KEY (commentID),
  FOREIGN KEY (postID) REFERENCES post(postID)
  );

CREATE TABLE `follow` (
  followID int NOT NULL AUTO_INCREMENT,
  follower int,
  followed int,
  PRIMARY KEY (followID),
  FOREIGN KEY (follower) REFERENCES users(usersID),
  FOREIGN KEY (followed) REFERENCES users(usersID)
  );

INSERT INTO post VALUES 
('','4','6.jpg','ini foto pertama','2022-04-25'),
('','4','7.jpg','ini foto kedua','2022-04-25'),
('','4','8.jpg','ini foto ketiga','2022-04-25'),
('','4','9.jpg','ini foto keempat','2022-04-25'),
('','4','10.jpg','ini foto kelima','2022-04-25');

insert into users (`usersID`,`name`,`username`,`email`,`password`) VALUES
(4,'Ujang ka','ujangbadai','ujang@gmail.com','$2y$10$0tIg45vtgTM0PGaIlC7yg.UhVfGwQzskHzg1TscJuqT0WtW7X68P.'),
(5,'ujang kedua','ujangkedua','ujang@gmail.com','$2y$10$lCzbI9WldnHsxEOZhPGkQucmsqMJSjyRAu2n8262ZAAkhoQRK42qq');

UPDATE `users` SET `bio` = 
'saya senang sekali bisa kulah di kampus upn jogjakarta karena disana saya bisa punya banyak teman. 
saya masih mahasiswa tingkat 1 karena saya masuk kuliah tahun 2021. nama saya ujan badai, 
saya ganteng seperti badai. kemampuan saya yaitu bisa berbahasa lorem, 
\"Lorem ipsum dolor, sit amet consectetur adipisicing elit. Commodi 
nesciunt officia quibusdam reiciendis ipsa quia facilis?\"' 
WHERE `users`.`usersID` = 5;