// tabs button
$(".nav li a").on("click", function () {
  $(".nav li a").removeClass("active");
  $(this).addClass("active");
});

// default snap
$(document).ready(function () {
  $("#snap-box").hide();
});

// close snap
$("#snap-bg").on("click", function () {
  $("#snap-box").hide();
});

// open snap
let snapList = document.querySelectorAll(".snap .snap-canvas");
snapList.forEach(function (listEl) {
  listEl.addEventListener("click", function (e) {
    e.preventDefault();
    $("#snap-box").show();
  });
});

// open post
let postList = content.querySelectorAll(".gallery li .thumbnail");
postList.forEach(function (postEl) {
  postEl.addEventListener("click", function (e) {
    e.preventDefault();
    let indexScr = parseInt(postEl.getAttribute("id")) + 1;
    $(".gallery li:nth-child(" + indexScr + ") .preview-container").show();
    $(".gallery li:nth-child(" + indexScr + ") .preview-container .post-container").show();
  });
});

// close post
let postBg = content.querySelectorAll(".gallery li .preview-container .post-bg");
postBg.forEach(function (postEl) {
  postEl.addEventListener("click", function (e) {
    e.preventDefault();
    $(".gallery li .preview-container").hide();
    $(".gallery li .preview-container .post-container").hide();
  });
});

// delete
const deleteLink = document.querySelector(".card-header .dropdown .dropdown-menu a");
const link = deleteLink.getAttribute("href");
deleteLink.addEventListener("click", function () {
  location.replace(link);
});
