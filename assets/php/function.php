<?php 
// config di deklarasikan sebelum pemanggilan require function

function profileHeader() {
    if (isset($_GET['value'])) {
        if ($_GET['value'] == "post" || $_GET['value'] == "saved" || $_GET['value'] == "tags") {
            return true;
        } else {
            return false;
        }
        return true;
    } else {
        return false;
    }
}

function deletePost($data) {
    //variabel global
    global $conn;

    // query
    mysqli_query($conn, "DELETE FROM post WHERE postID = '$data'");
    
    // return
    return mysqli_affected_rows($conn);
}

function createPost($data) {
    global $conn;

    $photo = upload("photo","create");
    if (!$photo) {
        return false;
    }
    $caption = $data["caption"];
    $usersID = $_SESSION["id"];
    $currentDate = date("Y-m-d");

    mysqli_query($conn, "INSERT INTO post VALUES (
        '',
        '$usersID',
        '$photo',
        '$caption',
        '$currentDate'
    )");

    return mysqli_affected_rows($conn);
}

function upload($inputName,$type) {
    $namaFile = $_FILES["$inputName"]["name"];
    $tmpName = $_FILES["$inputName"]["tmp_name"];
    $error = $_FILES["$inputName"]["error"];
    $size = $_FILES["$inputName"]["size"];

    // ketika user tidak menginputkan gambar
    if ($error == 4) {
        echo "
            <script>
                alert('Silahkan Pilih Gambar!');
            </script>
        ";
        return false;
    }

    // ketika user bukan memasukkan gambar
    $ekstensiFileValid = ['jpg','jpeg','png'];
    $ekstensiFileUser = explode('.',$namaFile);
    $ekstensiFileUser = strtolower(end($ekstensiFileUser)); // mengambil ekstensi file dengan cara explode, ambil array akhir, paksa menjadi huruf kecil
    if (!in_array($ekstensiFileUser, $ekstensiFileValid)) {
        echo "
            <script>
                alert('File yang anda masukkan bukan gambar!');
            </script>
        ";
        return false;        
    }

    // ketika file terlalu besar
    $ukuranMaksimal = 3000000; // 3000000 = 3MB
    if ($size > $ukuranMaksimal) {
        echo "
            <script>
                alert('File yang anda masukkan terlalu besar!');
            </script>
        ";
        return false;
    }

    // lulus upload
        // generate nama baru
        $namaFileBaru = uniqid(); //fungsi membuat karakter random
    $namaFileBaru .= '.'.$ekstensiFileUser;

    switch ($type) {
        case 'create':
            move_uploaded_file($tmpName, "../assets/img/posts/$namaFileBaru");
            break;
        
        case 'update':
            move_uploaded_file($tmpName, "../assets/img/profiles/$namaFileBaru");
            break;
    }

    return $namaFileBaru;
}

