<?php 
 
$server = "localhost";
$user = "root";
$pass = "";
$database = "finproweb";
 
$conn = mysqli_connect($server, $user, $pass, $database);
 
if (!$conn) {
    die("<script>alert('Gagal tersambung dengan database.')</script>");
}

function getQuery($params) {
    global $conn;
    $query=mysqli_query($conn, "$params");
   
    // exception handling
    if (!$query) {
        return mysqli_error($conn);
        exit;
    }

    // return
    $table = []; // membuat kotak array 
    while ($row = mysqli_fetch_assoc($query)) {
        $table[] = $row; // menambahkan tiap tiap baris
    }
    return $table; // mengembalikan array multi dimensi
}