<?php 

function login($params) {
    // mengambil variable
    $username = $params["username"];
    $password = $params["password"];

    // ambil username database
    $userOnDB = getQuery("SELECT * FROM users WHERE username = '$username'");

    // cek username         
    if (count($userOnDB) > 0) {
        
        // cek password
        if (password_verify($password, $userOnDB[0]["password"]) == 1) {
            return true;
        } else {
            echo "<span id='passWarning' class='warning text-danger'>Password salah!</span>";
            return false;
        }
    } else {
        echo "<span id='usernameWarning' class='warning text-danger'>Username salah!</span>";
    }
}

function register($params) {
    // global
    global $conn;

    // menangkap variable
    $username = strtolower(stripslashes($params["username"]));
    $displayName = mysqli_real_escape_string($conn, $params["displayName"]);
    $email = mysqli_real_escape_string($conn, $params["email"]);
    $password = mysqli_real_escape_string($conn, $params["password"]);
    $password2 = mysqli_real_escape_string($conn, $params["password2"]);

    // cek username
    $sameUser = getQuery("SELECT username FROM users WHERE username = '$username'");
    if ($sameUser) {
        echo "<span id='usernameWarning' class='warning text-danger'>Username tidak tersedia!</span>";
        return false;
    }

    // cek username dengan spasi
    for ( $idx = 0; $idx < strlen( $username ); $idx++ )
    if ( ctype_space( $username[$idx] ) ) {
        echo "<span id='usernameWarning' class='warning text-danger'>Username tidak boleh mengandung spasi!</span>";
        return false;
    }

    // cek email
    if (isset($email)) {
        $emailExplode = explode('@', $email, 2);
        if (count($emailExplode) == 1) {
            echo "<span id='emailWarning' class='warning text-danger'>yourEmail@gmail.com, yourEmail@yahoo.com, atau yang lainnya</span>";
            return false;
        }
    }

    // cek konfirmasi password
    if ($password != $password2) {
        echo "<span id='passWarning' class='warning text-danger'>konfirmasi password salah!</span>";
        return false;
    }

    // cek password kosong  
    if (($password2 || $password) == 0) {
        echo "<span id='passWarning' class='warning text-danger'>isi password terlebih dahulu!</span>";
        return false;
    }

    // enkripsi password
    $password = password_hash($password, PASSWORD_DEFAULT); // parameter 1 = string yg mau di acak, parameter 2 = metode mengacaknya

    // tambah data baru ke database
    mysqli_query($conn, "INSERT INTO users VALUES (
        '',
        '$displayName',
        '$username',
        '$email',
        '$password',
        '',
        'default.png',
        ''
    )");
    
    return mysqli_affected_rows($conn);
}

function updateUser($params) {
    // global
    global $conn;

    // menangkap variable
    $oldusername = $_SESSION["user"];
    if (empty($_FILES["profilePicture"]["name"])) {
        $profile_photo = getQuery("SELECT profile_photo FROM users WHERE username = '$oldusername'")[0]['profile_photo'];
    } else {
        $profile_photo = upload("profilePicture","update");
    }
    $username = strtolower(stripslashes($params["username"]));
    $name = mysqli_real_escape_string($conn, $params["dname"]);
    $bio = mysqli_real_escape_string($conn, $params["bio"]);
    $link = mysqli_real_escape_string($conn, $params["userWebsite"]);

    // cek username dengan spasi
    for ( $idx = 0; $idx < strlen( $username ); $idx++ )
    if ( ctype_space( $username[$idx] ) ) {
        echo "<span id='usernameWarning' class='warning text-danger'>Username tidak boleh mengandung spasi!</span>";
        return false;
    }

    if ($username == $oldusername) {
        // tambah data baru ke database
        mysqli_query($conn, "UPDATE users SET 
            profile_photo = '$profile_photo',
            users.name = '$name',
            bio = '$bio',
            link = '$link'
            WHERE  username = '$oldusername';
        ");
    } else {
        // tambah data baru ke database
        mysqli_query($conn, "UPDATE users SET 
            profile_photo = '$profile_photo',
            username = '$username',
            users.name = '$name',
            bio = '$bio',
            link = '$link'
            WHERE  username = '$oldusername';
        ");
    }   
    
    return mysqli_affected_rows($conn);
}

function setUserSession() {
    // set session
    $_SESSION["login"] = true;
    $_SESSION["user"] = $_POST["username"];
    $username = $_SESSION["user"];
    $_SESSION["id"] = getQuery("SELECT usersID FROM users WHERE username = '$username'")[0]['usersID'];
}


// logic
if (isset($_POST["login-submit"])) { // login process
    if (login($_POST) > 0) {
        // feedback
        echo "<script>alert('berhasil login')</script>";
        // set session
        setUserSession();
        // relocation
        header("location: index.php");
    } else {
        mysqli_error($conn);
    }
}

if (isset($_POST["register-submit"])) { // register process
    if (register($_POST) > 0) {
        echo "<script>alert('berhasil mendaftar')</script>";
        // set session
        setUserSession();
        // relocation
        header("location: index.php");
    } else {
        mysqli_error($conn);
    }
}

