<?php 
session_start();
require '../assets/php/config.php';
require '../assets/php/authorization.php';
require '../assets/php/function.php';
require '../assets/php/login-register.php';

// set profile
$profileInfo = getQuery("SELECT * FROM users WHERE username = '$user'")[0];

// edit profile
if (isset($_POST["edit-submit"])) {
    if (updateUser($_POST) > 0) {
        echo "<script>alert('berhasil mengubah data!')</script>";
        header("location: profile.php");
    } else {
        echo mysqli_error($conn);
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="edit-profile.css">
    <link rel="stylesheet" href="../assets/css/reset.css">
    <link rel="stylesheet" href="../assets/css/scrollbar.css">

    <!-- Fontawesome CSS -->
    <script src="https://kit.fontawesome.com/c101a12428.js" crossorigin="anonymous"></script>

    <title>Profile</title>
</head>

<body>
    <div class="container-lg my-5">
        
        <!-- edit profile -->
        <div class="card text-dark bg-light mb-3">
            <form action="" method="post" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="profile-picture mb-3 mx-auto rounded-circle">
                    
                    </div>
                    <div class="mb-3">
                      <label for="profilePicture" class="form-label">Profile Picture</label>
                      <input type="file" class="form-control" name="profilePicture" value="<?= $profileInfo["profile_photo"] ?>" id="profilePicture" placeholder=".jpg .png. jpeg" aria-describedby="fileHelpId">
                      <small id="fileHelpId" class="form-text text-muted">your file must be .jpg .jpeg .png</small>
                    </div>
                    <div class="mb-3">
                      <label for="username" class="form-label">Username</label>
                      <input type="text" class="form-control" name="username" id="username" value="<?= $profileInfo["username"] ?>" aria-describedby="helpId" placeholder="">
                      <small id="helpId" class="form-text text-muted">https://wwww.socialmedia.com?id=yourusername</small>
                    </div>
                    <div class="mb-3">
                        <label for="dname" class="form-label">Display Name</label>
                        <input type="text" class="form-control" name="dname" id="dname" value="<?= $profileInfo["name"] ?>" aria-describedby="helpId" placeholder="">
                        <small id="helpId" class="form-text text-muted">input your new display name</small>
                    </div>
                    <div class="mb-3">
                        <label for="nouns" class="form-label">Pronouns</label>
                        <select class="form-control" name="nouns" id="nouns" aria-describedby="helpId" placeholder="">
                            <option selected>add your pronouns</option>
                            <option value="he/him">he/him</option>
                            <option value="ey/em">ey/em</option>
                            <option value="ne/nem">ne/nem</option>
                            <option value="she/her">she/her</option>
                            <option value="they/them">hthey/them</option>
                        </select>
                        <small id="helpId" class="form-text text-muted">Choose up to 2 sets of pronouns to appear on your profile so others know how to refer to you. You can edit or remove these any time</small>
                    </div>
                    <div class="mb-3">
                        <label for="bio" class="form-label">About</label>
                        <textarea type="text" class="form-control" name="bio" id="bio" aria-describedby="helpId" placeholder="wanna say something?" rows="4">
                            <?= $profileInfo["bio"] ?>
                        </textarea>
                    </div>
                    <div class="mb-3">
                        <label for="userWebsite" class="form-label">Website</label>
                        <input type="text" class="form-control" name="userWebsite" id="userWebsite" value="<?= $profileInfo["link"] ?>" aria-describedby="helpId" placeholder="">
                        <small id="helpId" class="form-text text-muted">active website</small>
                    </div>
                </div>
                <div class="card-footer text-muted">
                    <div class="d-grid">
                        <div class="row gap-2 justify-content-center">
                            <a href="profile.php?value=post" name="previous" id="previous" class="w-25 btn btn-outline-primary">back</a>
                            <button type="submit" name="edit-submit" id="edit-submit" class="w-25 btn btn-primary">submit</button>
                        </div>
                    </div>
                </div>  
            </form>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
    <!-- Optional JavaScript; choose one of the two! -->
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/profile-javascript.js"></script>

</body>

</html>