<?php 
session_start();
require '../assets/php/config.php';
require '../assets/php/authorization.php';
require '../assets/php/function.php';

// validasi link
if (profileHeader() < 1) {
    header("location: profile.php?value=post");
}

// visit as guest
if (isset($_GET['g'])) {
    // set username
    $householder = $_GET['g'];
    $householder = getQuery("SELECT username FROM users WHERE usersID = '$householder'")[0]["username"];
    $guest = $_SESSION['user'];
    // set profile
    $profileInfo = getQuery("SELECT * FROM users WHERE username = '$householder'")[0];
} else {
    // set username
    $householder = $_SESSION["user"];
    // set profile
    $profileInfo = getQuery("SELECT * FROM users WHERE username = '$householder'")[0];
}

// set isi postingan
if ($_GET["value"] == "saved") {
    // belom selesai logic nya
    $postInfo = '';
} elseif ($_GET["value"] == "tags") {
    // belom selesai logic nya
    $postInfo = '';
} else {
    $postInfo = getQuery("SELECT * FROM post JOIN users USING (usersID) WHERE username = '$householder'");
}
$user = $_SESSION['id'];
$follower = getQuery("SELECT * FROM follow WHERE followed=$user");
$followed = getQuery("SELECT * FROM follow WHERE follower=$user");
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap"
        rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="../assets/css/profile.css">
    <link rel="stylesheet" href="../assets/css/index.css">

    <style>
    .dropdown-toggle::after {
        content: none;
    }
    </style>

    <!-- Fontawesome CSS -->
    <script src="https://kit.fontawesome.com/c101a12428.js" crossorigin="anonymous"></script>

    <title>Profile</title>
</head>

<body>
    <!-- Navbar -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container justify-content-center">
                <div class="d-flex flex-row justify-content-between align-items-center col-9">
                    <a class="navbar-brand" href="#">
                        <img src="../assets/img/ig-logo.png" alt="" loading="lazy" />
                    </a>
                    <div>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control me-sm-2" type="search" placeholder="Search"
                                aria-label="Search" />
                        </form>
                    </div>
                    <div class="d-flex flex-row">
                        <ul class="list-inline m-0">
                            <li class="list-inline-item ms-2">
                                <a href="index.php" class="link-menu">
                                    <svg aria-label="Home" class="_9zn7" color="#262626" fill="#262626" height="24"
                                        role="img" viewBox="0 0 24 24" width="24">
                                        <path
                                            d="M22 23h-6.001a1 1 0 01-1-1v-5.455a2.997 2.997 0 10-5.993 0V22a1 1 0 01-1 1H2a1 1 0 01-1-1V11.543a1.002 1.002 0 01.31-.724l10-9.543a1.001 1.001 0 011.38 0l10 9.543a1.002 1.002 0 01.31.724V22a1 1 0 01-1 1z">
                                        </path>
                                    </svg>
                                </a>
                            </li>
                            <?php if (!isset($guest)) : ?>
                            <li class="list-inline-item ms-2">
                                <a href="post.php" class="link-menu">
                                    <svg aria-label="New post" class="_9zn7" color="#262626" fill="#262626" height="24"
                                        role="img" viewBox="0 0 24 24" width="24">
                                        <path
                                            d="M2 12v3.45c0 2.849.698 4.005 1.606 4.944.94.909 2.098 1.608 4.946 1.608h6.896c2.848 0 4.006-.7 4.946-1.608C21.302 19.455 22 18.3 22 15.45V8.552c0-2.849-.698-4.006-1.606-4.945C19.454 2.7 18.296 2 15.448 2H8.552c-2.848 0-4.006.699-4.946 1.607C2.698 4.547 2 5.703 2 8.552z"
                                            fill="none" stroke="currentColor" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-width="2"></path>
                                        <line fill="none" stroke="currentColor" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-width="2" x1="6.545" x2="17.455" y1="12.001"
                                            y2="12.001"></line>
                                        <line fill="none" stroke="currentColor" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-width="2" x1="12.003" x2="12.003" y1="6.545"
                                            y2="17.455"></line>
                                    </svg>
                                </a>
                            </li>
                            <?php endif; ?>
                            <li class="list-inline-item ms-2 align-middle dropdown">
                                <a class="link-menu dropdown-toggle hide-arrow" href="javascript:void(0);"
                                    data-bs-toggle="dropdown">
                                    <div
                                        class="rounded-circle overflow-hidden d-flex justify-content-center align-items-center border topbar-profile-photo ratio ratio-1x1">
                                        <img src="../assets/img/profiles/<?= $profileInfo['profile_photo'] ?>" alt="">
                                    </div>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    <li>
                                        <a class="dropdown-item" href="profile.php">
                                            <span class="align-middle">My Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item link link-danger" href="logout.php">
                                            <span class="align-middle">Logout</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <!-- profile info -->
    <div class="container-lg">
        <!-- profile info -->
        <div class="profile-head d-flex justify-content-between align-items-center flex-column mt-5 mb-3">
            <div class="profile-image ratio ratio-1x1 overflow-hidden">
                <img src="../assets/img/profiles/<?= $profileInfo['profile_photo'] ?>" alt="">
            </div>
            <div class="display-name fw-bold">
                <h2 class="fs-2"><?= $profileInfo["name"] ?></h2>
            </div>
            <div class="username fw-light">
                <h6><?= $profileInfo["username"] ?> &#149 he/him</h6>
            </div>
            <div class="profile-info d-flex justify-content-between align-items-center">
                <h5 class="fs-5"><span class="fw-bold">
                        <?php
                        if (empty($postInfo)) {
                            echo 0;
                        } else {
                            echo count($postInfo); 
                        }
                    ?>
                    </span> post</h5>
                <h5 class="fs-5"><span class="fw-bold"><?php
                        if (empty($follower)) {
                            echo 0;
                        } else {
                            echo count($follower); 
                        }
                    ?> </span> followers</h5>
                <h5 class="fs-5"><span class="fw-bold"><?php
                        if (empty($followed)) {
                            echo 0;
                        } else {
                            echo count($followed); 
                        }
                    ?></span> following</h5>
            </div>
            <?php if ($profileInfo["bio"]) : ?>
            <div class="profile-bio text-justify fw-light">
                <!-- set profioe-bio max 40 word -->
                <p class="lh-sm">
                    <?= $profileInfo["bio"] ?>
                </p>
                <span class="profile-link d-inline-block mt-1"><a href="#"
                        class="text-decoration-none fw-bold link-primary"><?= $profileInfo["link"] ?></a></span>
            </div>
            <?php endif ?>
            <?php if (!isset($guest)) : ?>
            <div class="profile-edit" id="profile-edit">
                <a name="profile-edit" id="" class="btn btn-light border border-1 rounded-3 mt-2 link-secondary"
                    href="edit-profile.php" role="button">edit profile</a>
            </div>
            <?php else : ?>
            <div class="profile-follow" id="profile-follow">
                <a name="profile-follow" id="" class="btn btn-primary border border-1 rounded-3 mt-2 px-3"
                    href="follow.php" role="button">follow</a>
            </div>
            <?php endif; ?>
        </div>

        <!-- highlights -->
        <div class="snap d-flex align-items-center w-100 px-5">
            <!-- change the href -->
            <a class="snap-canvas mx-4 rounded-circle position-relative d-inline-block link-dark" href="#" id="snap-1">
                <!-- change the canvas id -->
                <div class="snap-thumbnail rounded-circle position-relative start-50 top-50 translate-middle"></div>
                <!-- change the label id -->
                <label for="snap-1" class="position-relative start-50 translate-middle-x">Location</label>
            </a>
            <!-- change the href -->
            <a class="snap-canvas mx-4 rounded-circle position-relative d-inline-block link-dark" href="#" id="snap-2">
                <!-- change the canvas id -->
                <div class="snap-thumbnail rounded-circle position-relative start-50 top-50 translate-middle"></div>
                <!-- change the label id -->
                <label for="snap-2" class="position-relative start-50 translate-middle-x">Vacation</label>
            </a>
        </div>

        <!-- tabs -->
        <hr id="bd" class="d-inline-block mt-5">
        <div class="tabs d-flex justify-content-center align-items-center w-100">
            <ul class="nav nav-tabs nav-justified justify-content-center w-75" id="tab-list">
                <li class="nav-item" id="tab-post">
                    <a class="nav-link link-secondary" aria-current="page" <?php if (!isset($guest)) : ?>
                        href="profile.php?value=post#bd" <?php else : ?>
                        href="profile.php?value=post&g=<?= $profileInfo['usersID'] ?>" <?php endif; ?>>
                        <i class="fa-solid fa-grip me-1"></i>
                        Post
                    </a>
                </li>
                <?php if (!isset($guest)) : ?>
                <li class="nav-item" id="tab-saved">
                    <a class="nav-link link-secondary" href="profile.php?value=saved#bd">
                        <i class="fa-solid fa-bookmark me-1"></i>
                        Saved
                    </a>
                </li>
                <?php endif; ?>
                <li class="nav-item" id="tab-tag">
                    <a class="nav-link link-secondary" <?php if (!isset($guest)) : ?> href="profile.php?value=tags#bd"
                        <?php else : ?> href="profile.php?value=tags&g=<?= $profileInfo['usersID'] ?>" <?php endif; ?>>
                        <i class="fa fa-tag me-1" aria-hidden="true"></i>
                        Tags
                    </a>
                </li>
            </ul>
        </div>

        <!-- content -->
        <div class="content d-flex justify-content-center align-items-center w-100" id="content">
            <?php if ($postInfo) : ?>
            <ul class="gallery">
                <?php $index = 0 ?>
                <?php foreach ($postInfo as $post) : ?>
                <li>
                    <a href="profile.php?value=<?= $_GET["value"] ?>" class="thumbnail" id="<?= $index ?>">
                        <div class="ratio ratio-1x1">
                            <img class="" src="../assets/img/posts/<?= $post["photo"] ?>" alt="">
                        </div>
                    </a>

                    <!-- post preview -->
                    <div class="preview-container position-fixed top-0 bottom-0 start-0 end-0"
                        style="display:none; z-index: 10;" id="postP-box">
                        <div class="post-bg position-fixed top-0 bottom-0 start-0 end-0 preview-bg bg-dark opacity-75"
                            id="postP-bg"></div>
                        <!-- Posts -->
                        <div class="post-container w-75 position-fixed start-50 top-50 translate-middle"
                            style="display:none">
                            <div class="card overflow-hidden">
                                <div class="row">
                                    <div class="col p-0">
                                        <div class="ratio ratio-1x1">
                                            <img src="../assets/img/posts/<?= $post["photo"] ?>" alt="" />
                                        </div>
                                    </div>
                                    <div class="col p-0 d-flex flex-column">
                                        <div class="card-header">
                                            <div class="d-flex flex-row align-items-center">
                                                <div
                                                    class="rounded-circle overflow-hidden d-flex justify-content-center align-items-center border post-profile-photo me-2 my-2">
                                                    <img src="../assets/img/profiles/mpit.jpg" />
                                                </div>
                                                <span class="fw-bold flex-grow-1"><?= $profileInfo["username"] ?></span>
                                                <?php if (!isset($guest)) : ?>
                                                <!-- dropdown delete -->
                                                <div class="dropdown open">
                                                    <a class="btn btn-outline p-0" type="button" id="triggerId"
                                                        data-bs-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                        <i class="fa fa-ellipsis-h fs-5 d-inline-block px-3"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                    <div class="dropdown-menu" aria-labelledby="triggerId">
                                                        <a class="dropdown-item text-danger"
                                                            href="../assets/php/delete-post.php?in=<?= $post['postID'] ?>">Delete</a>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="card-body p-0 flex-grow-1">
                                            <!-- Post Body -->
                                            <div class="px-3 d-flex flex-column">
                                                <div class="caption">
                                                    <span class="mb-3"><strong
                                                            class="d-inline-block fw-bold my-3"><?= $profileInfo["username"] ?></strong>
                                                        <?= $post["caption"] ?></span>
                                                </div>
                                                <div class="">
                                                    <div class="mb-3">
                                                        <span class=""><strong class="d-inline-block fw-bold">kittendust
                                                            </strong> Asik
                                                            mantap kak aweaweaweaweaweaweaweawewae</span>
                                                    </div>
                                                    <div class="mb-3">
                                                        <span class=""><strong class="d-inline-block fw-bold">tulus
                                                            </strong> ku kira
                                                            kita akan bersama</span>
                                                    </div>
                                                    <a class="btn p-0 d-block mb-3 text-start">
                                                        <span class="text-muted">View all 999 comments</span>
                                                    </a>
                                                </div>
                                                <strong class="d-block mb-2">999.999 likes</strong>
                                                <small
                                                    class="d-inline-block text-muted text-uppercase time-stamp mb-3"><?= date("d F Y", strtotime($post["date"])) ?></small>
                                            </div>
                                            <!-- End Post Body -->

                                        </div>
                                        <div class="card-footer p-0">
                                            <!-- Add Comment -->
                                            <div class="position-relative border-top bottom-0">
                                                <form>
                                                    <input class="w-100 form-control border-0 py-2" type="text"
                                                        name="comment" placeholder="Add a comment..."
                                                        autocomplete="off">
                                                    <button
                                                        class="btn py-2 me-3 fw-bold text-primary position-absolute comment-post top-0"
                                                        name="comment-submit">Post</button>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- End Add Comment -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Posts -->
                    </div>
                </li>

                <?php $index++; ?>
                <?php endforeach; ?>
            </ul>
            <?php else : ?>
            <p class="d-inline-block w-100 text-center text-muted p-5">no post available</p>
            <?php endif; ?>
        </div>

    </div>

    <!-- snap view -->
    <div class="snap-box position-fixed start-0 end-0 top-0 bottom-0" style="display:none" id="snap-box">
        <div class="snap-bg position-absolute start-0 end-0 top-0 bottom-0 bg-dark opacity-75" id="snap-bg"></div>
        <div class="snap-element">
            <div class="snap-frame position-absolute start-50 top-50 translate-middle">

            </div>
        </div>
    </div>


    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <!-- Optional JavaScript; choose one of the two! -->
    <script src="../assets/js/jquery-3.6.0.min.js"></script>
    <script src="../assets/js/profile-javascript.js"></script>

    <!-- javascript for tabs -->
    <?php if ($_GET['value'] == 'post') : ?>
    <script>
    document.querySelectorAll("#tab-list li a")[0].classList.add("active");
    </script>
    <?php elseif ($_GET['value'] == 'saved') : ?>
    <script>
    document.querySelectorAll("#tab-list li a")[1].classList.add("active");
    </script>
    <?php elseif ($_GET['value'] == 'tags') : ?>
    <script>
    document.querySelector("#tab-list li:last-child a").classList.add("active");
    </script>
    <?php endif ?>

</body>

</html>