<?php 
session_start();
require '../assets/php/config.php';
require '../assets/php/login-register.php';
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<body>
    <section class="vh-100 bg-light">
        <div class="container py-5 h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                    <div class="card bg-white text-dark" style="border-radius: 1rem;">
                        <div class="card-body p-5 text-center">

                            <div class="mb-md-5 mt-md-4 pb-5">

                                <h2 class="fw-bold mb-2 text-uppercase">Login</h2>
                                <p class="text-white-50 mb-5">Please enter your login and password!</p>

                                <form id="formAuthentication" class="mb-3" action="" method="POST">
                                    <div class="d-flex justify-content-start">
                                        <label class="form-label" for="username">Username</label>
                                    </div>
                                    <div class="mb-3" id="username-parent">
                                        <input type="text" class="form-control" id="username" name="username"
                                            placeholder="Enter your username" autofocus />
                                        <span id='usernameWarning' class='warning'></span>
                                        <!-- tag ini akan diisi ketika terjadi error -->
                                    </div>
                                    <div class="mb-3 form-password-toggle" id="password-parent">
                                        <div class="d-flex justify-content-start">
                                            <label class="form-label" for="password">Password</label>
                                        </div>
                                        <div class="mb-3">
                                            <input type="password" id="password" class="form-control" name="password"
                                                placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                                aria-describedby="password" />
                                            <span id='passWarning' class='warning'></span>
                                            <!-- tag ini akan diisi ketika terjadi error -->
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <button class="btn btn-primary d-grid w-100" name="login-submit"
                                            type="submit">Sign in</button>
                                    </div>
                                </form>

                            </div>

                            <div>
                                <p class="mb-0">Don't have an account? <a href="register.php"
                                        class="text-primary-50 fw-bold text-decoration-none">Sign
                                        Up</a>
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Page JS -->
    <script>
    // username warning
    const unameWarning = document.getElementById("usernameWarning");
    const uname = document.getElementById("username-parent");
    uname.appendChild(unameWarning);

    // password warning
    const passWarning = document.getElementById("passWarning");
    const pass = document.getElementById("password-parent");
    pass.appendChild(passWarning);
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

</body>

</html>