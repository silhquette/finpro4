<?php 
session_start();
require_once '../assets/php/config.php';
require '../assets/php/authorization.php';

// set profile
$user = $_SESSION['user'];
$profileInfo = getQuery("SELECT * FROM users WHERE username = '$user'")[0];
$postInfo = getQuery("SELECT * FROM post JOIN users USING (usersID) ORDER BY postID DESC");
$commentInfo = getQuery("SELECT * FROM comment JOIN post USING (postID)");
$usersInfo = getQuery("SELECT * FROM users WHERE username != '$user' LIMIT 5 ");
$followInfo = getQuery("SELECT * FROM follow JOIN users USING (usersID)");
$followDB = getQuery("SELECT * FROM follow");


if(isset($_POST['comment'])){
    $commentUser = $_SESSION['user'];
    $commentPost = $_POST['id'];
    $content = $_POST['content'];

    $sql = "INSERT INTO `comment` (commentID,commentUser,postID,content)
    VALUES ('','$commentUser','$commentPost','$content')";
    $result = mysqli_query($conn,$sql);

    header("location: index.php");
}

if(isset($_POST['follow'])){
    $followed = $_POST['followed'];
    $follower = $_POST['follower'];

    $sql = "INSERT INTO `follow` (`followID`,`followed`,`follower`)
    VALUES ('','$followed','$follower')";
    $result = mysqli_query($conn,$sql);

    header("location: index.php");
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />

    <!-- Main CSS -->
    <link rel="stylesheet" href="../assets/css/index.css" />

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet" />

    <style>
    .dropdown-toggle::after {
        content: none;
    }
    </style>

    <title>Hello, world!</title>
</head>

<body>
    <!-- Navbar -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container justify-content-center">
                <div class="d-flex flex-row justify-content-between align-items-center col-9">
                    <a class="navbar-brand" href="#">
                        <img src="../assets/img/ig-logo.png" alt="" loading="lazy" />
                    </a>
                    <div>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control me-sm-2" type="search" placeholder="Search"
                                aria-label="Search" />
                        </form>
                    </div>
                    <div class="d-flex flex-row">
                        <ul class="list-inline m-0">
                            <li class="list-inline-item ms-2">
                                <a href="index.php" class="link-menu">
                                    <svg aria-label="Home" class="_9zn7" color="#262626" fill="#262626" height="24"
                                        role="img" viewBox="0 0 24 24" width="24">
                                        <path
                                            d="M22 23h-6.001a1 1 0 01-1-1v-5.455a2.997 2.997 0 10-5.993 0V22a1 1 0 01-1 1H2a1 1 0 01-1-1V11.543a1.002 1.002 0 01.31-.724l10-9.543a1.001 1.001 0 011.38 0l10 9.543a1.002 1.002 0 01.31.724V22a1 1 0 01-1 1z">
                                        </path>
                                    </svg>
                                </a>
                            </li>
                            <li class="list-inline-item ms-2">
                                <a href="post.php" class="link-menu">
                                    <svg aria-label="New post" class="_9zn7" color="#262626" fill="#262626" height="24"
                                        role="img" viewBox="0 0 24 24" width="24">
                                        <path
                                            d="M2 12v3.45c0 2.849.698 4.005 1.606 4.944.94.909 2.098 1.608 4.946 1.608h6.896c2.848 0 4.006-.7 4.946-1.608C21.302 19.455 22 18.3 22 15.45V8.552c0-2.849-.698-4.006-1.606-4.945C19.454 2.7 18.296 2 15.448 2H8.552c-2.848 0-4.006.699-4.946 1.607C2.698 4.547 2 5.703 2 8.552z"
                                            fill="none" stroke="currentColor" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-width="2"></path>
                                        <line fill="none" stroke="currentColor" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-width="2" x1="6.545" x2="17.455" y1="12.001"
                                            y2="12.001"></line>
                                        <line fill="none" stroke="currentColor" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-width="2" x1="12.003" x2="12.003" y1="6.545"
                                            y2="17.455"></line>
                                    </svg>
                                </a>
                            </li>
                            <li class="list-inline-item ms-2 align-middle dropdown">
                                <a class="link-menu dropdown-toggle hide-arrow" href="javascript:void(0);"
                                    data-bs-toggle="dropdown">
                                    <div
                                        class="ratio ratio-1x1 rounded-circle overflow-hidden d-flex justify-content-center align-items-center border topbar-profile-photo">
                                        <img src="../assets/img/profiles/<?= $profileInfo['profile_photo'] ?>" />
                                    </div>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    <li>
                                        <a class="dropdown-item" href="profile.php">
                                            <span class="align-middle">My Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item link link-danger" href="logout.php">
                                            <span class="align-middle">Logout</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <!-- End Navbar -->

    <main>
        <!-- Stories -->
        <div class="mt-4">
            <div class="container d-flex justify-content-center">
                <div class="col-9">
                    <div class="row">
                        <div class="col-8">
                            <div class="card">
                                <div class="card-body d-flex justify-content-start">
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item">
                                            <button class="btn p-0 m-0">
                                                <div class="d-flex flex-column align-items-center">
                                                    <div
                                                        class="ratio ratio-1x1 rounded-circle overflow-hidden d-flex justify-content-center align-items-center border story-profile-photo">
                                                        <img src="../assets/img/profiles/mpit.jpg" />
                                                    </div>
                                                    <small>user 1</small>
                                                </div>
                                            </button>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <!-- End Stories -->

                            <!-- Posts -->
                            <div class="d-flex flex-column my-4">
                                <?php foreach((array) $postInfo as $post) : ?>
                                <?php 
                                    $tempID = $post['postID']; 
                                    $commentInfo = getQuery("SELECT * FROM comment WHERE postID=$tempID"); 
                                    ?>
                                <div class="card mb-3">
                                    <div class="card-header">
                                        <div class="d-flex flex-row align-items-center">
                                            <div
                                                class="rounded-circle overflow-hidden d-flex justify-content-center align-items-center border post-profile-photo me-3 ratio ratio-1x1">
                                                <img src="../assets/img/profiles/<?= $post['profile_photo'] ?>" />
                                            </div>
                                            <span class="fw-bold"><?= $post['username'] ?></span>
                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                        <div class="ratio ratio-1x1">
                                            <img src="../assets/img/posts/<?= $post["photo"] ?>" alt="" />
                                        </div>
                                        <div class="d-flex justify-content-between px-3 pt-3 pb-1">
                                            <ul class="list-inline mb-0">
                                                <li class="list-inline-item">
                                                    <button class="btn p-0">
                                                        <svg aria-label="Like" class="_9zn7" color="#262626"
                                                            fill="#262626" height="24" role="img" viewBox="0 0 24 24"
                                                            width="24">
                                                            <path
                                                                d="M16.792 3.904A4.989 4.989 0 0121.5 9.122c0 3.072-2.652 4.959-5.197 7.222-2.512 2.243-3.865 3.469-4.303 3.752-.477-.309-2.143-1.823-4.303-3.752C5.141 14.072 2.5 12.167 2.5 9.122a4.989 4.989 0 014.708-5.218 4.21 4.21 0 013.675 1.941c.84 1.175.98 1.763 1.12 1.763s.278-.588 1.11-1.766a4.17 4.17 0 013.679-1.938m0-2a6.04 6.04 0 00-4.797 2.127 6.052 6.052 0 00-4.787-2.127A6.985 6.985 0 00.5 9.122c0 3.61 2.55 5.827 5.015 7.97.283.246.569.494.853.747l1.027.918a44.998 44.998 0 003.518 3.018 2 2 0 002.174 0 45.263 45.263 0 003.626-3.115l.922-.824c.293-.26.59-.519.885-.774 2.334-2.025 4.98-4.32 4.98-7.94a6.985 6.985 0 00-6.708-7.218z">
                                                            </path>
                                                        </svg>
                                                    </button>
                                                </li>
                                                <li class="list-inline-item ms-2">
                                                    <button class="btn p-0">
                                                        <svg aria-label="Comment" class="_9zn7" color="#262626"
                                                            fill="#262626" height="24" role="img" viewBox="0 0 24 24"
                                                            width="24">
                                                            <path d="M20.656 17.008a9.993 9.993 0 10-3.59 3.615L22 22z"
                                                                fill="none" stroke="currentColor"
                                                                stroke-linejoin="round" stroke-width="2"></path>
                                                        </svg>
                                                    </button>
                                                </li>
                                                <li class="list-inline-item ms-2">
                                                    <button class="btn p-0">
                                                        <svg aria-label="Share Post" class="_9zn7" color="#262626"
                                                            fill="#262626" height="24" role="img" viewBox="0 0 24 24"
                                                            width="24">
                                                            <line fill="none" stroke="currentColor"
                                                                stroke-linejoin="round" stroke-width="2" x1="22"
                                                                x2="9.218" y1="3" y2="10.083"></line>
                                                            <polygon fill="none"
                                                                points="11.698 20.334 22 3.001 2 3.001 9.218 10.084 11.698 20.334"
                                                                stroke="currentColor" stroke-linejoin="round"
                                                                stroke-width="2"></polygon>
                                                        </svg>
                                                    </button>
                                                </li>
                                            </ul>
                                            <div>
                                                <button class="btn p-0">
                                                    <svg aria-label="Save" class="_9zn7" color="#262626" fill="#262626"
                                                        height="24" role="img" viewBox="0 0 24 24" width="24">
                                                        <polygon fill="none" points="20 21 12 13.44 4 21 4 3 20 3 20 21"
                                                            stroke="currentColor" stroke-linecap="round"
                                                            stroke-linejoin="round" stroke-width="2"></polygon>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <!-- Post Body -->
                                        <div class="px-3 pb-2">
                                            <strong class="d-block">999.999 likes</strong>
                                            <strong class=""><?= $post['username'] ?></strong>
                                            <span class="mb-1"><?= $post['caption'] ?></span>
                                            <button class="btn p-0 d-block">
                                                <span class="text-muted">View all comments</span>
                                            </button>
                                            <div>
                                                <?php foreach($commentInfo as $comment) : ?>
                                                <div>
                                                    <strong><?= $comment['commentUser'] ?></strong>
                                                    <span><?= $comment['content'] ?></span>
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <small class="text-muted text-uppercase"> <?= $post['date'] ?></small>
                                        </div>
                                        <!-- End Post Body -->
                                        <!-- Add Comment -->
                                        <div class="position-relative border-top">
                                            <form method="POST">
                                                <input class="w-100 border-0 p-3" name="content" type="text"
                                                    placeholder="Add a comment..." />
                                                <input type="text" name="id" value="<?= $post['postID'] ?>" hidden>
                                                <button class="btn fw-bold text-primary position-absolute comment-post"
                                                    type="submit" name="comment">Post</button>
                                            </form>
                                        </div>
                                        <!-- End Add Comment -->
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <!-- End Posts -->
                        </div>
                        <div class="col-4">
                            <div class="d-flex flex-row align-items-center">
                                <div
                                    class="rounded-circle overflow-hidden d-flex justify-content-center align-items-center side-profile-photo ratio ratio-1x1 ">
                                    <img src="../assets/img/profiles/<?= $profileInfo['profile_photo'] ?>" />
                                </div>
                                <div class="ms-3">
                                    <span class="d-block fw-bold"><?= $profileInfo['username'] ?></span>
                                    <span class="side-profile-name"><?= $profileInfo['name'] ?></span>
                                </div>
                            </div>
                            <div class="mt-3">
                                <div class="d-flex flex-row justify-content-between">
                                    <small>Suggestions For You</small>
                                    <small class="text-primary">See All</small>
                                </div>
                                <!-- Suggestion -->

                                <?php foreach($usersInfo as $users) : 
                                    $tempFollow = $users['usersID'];
                                    if($followInfo!=NULL){
                                        $followInfo = getQuery("SELECT * FROM follow WHERE followed = '$tempFollow'");
                                    }
                                    // var_dump($followInfo);
                                        ?>
                                <div>
                                    <form method="POST">
                                        <div class="d-flex flex-row justify-content-between my-3">

                                            <div class="d-flex flex-row align-items-center">
                                                <div
                                                    class="rounded-circle overflow-hidden d-flex justify-content-center align-items-center suggestion-profile-photo ratio ratio-1x1">
                                                    <img src="../assets/img/profiles/<?= $users['profile_photo'] ?>" />
                                                </div>
                                                <a href="profile.php?value=post&g=<?= $users['usersID'] ?>"
                                                    class="link link-dark text-decoration-none"><strong
                                                        class="ms-2"><?= $users['username'] ?></strong></a>
                                            </div>
                                            <input type="text" name="follower" value="<?= $profileInfo['usersID'] ?>"
                                                hidden>
                                            <input type="text" name="followed" value="<?= $users['usersID'] ?>" hidden>

                                            <?php if($followInfo!=NULL): ?>
                                            <?php 
                                            foreach($followInfo as $follow)
                                            if($followInfo[0]['follower']==$profileInfo['usersID']):
                                                $followBool = TRUE;
                                            else:
                                                $followBool = FALSE;
                                            endif; 
                                            else: 
                                                $followBool = FALSE;

                                            endif; ?>
                                            <?php if($followBool): ?>
                                            <button
                                                class="btn btn-sm text-primary fw-bold shadow-none text-muted">Followed</button>
                                            <?php else: ?>
                                            <button class="btn btn-sm text-primary fw-bold shadow-none" type="submit"
                                                name="follow">Follow</button>
                                            <?php endif; ?>








                                            <!-- <button class="btn btn-sm text-primary fw-bold shadow-none" type="submit"
                                                name="follow">Follow</button>
                                            <button
                                                class="btn btn-sm text-primary fw-bold shadow-none text-muted">Followed</button> -->
                                        </div>
                                    </form>
                                </div>
                                <?php endforeach; ?>
                                <!-- End Suggestion -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
</body>

</html>