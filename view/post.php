<?php 
session_start();
require '../assets/php/config.php';
require '../assets/php/function.php';

if (isset($_POST["post-submit"])) {
    if (createPost($_POST) > 0) {
        echo "<script>alert('berhasil post')</script>";
        header('location: profile.php');
    }
}


// set username
$user = $_SESSION["user"];
$profileInfo = getQuery("SELECT * FROM users WHERE username = '$user'")[0];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="../assets/css/profile.css">
    <link rel="stylesheet" href="../assets/css/index.css">

</head>

<body>
    <div class="post-container container-lg w-75 position-fixed start-50 top-50 translate-middle">
        <div class="card">
            <form method="POST" action="" enctype="multipart/form-data">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <a href="index.php" class="link link-secondary text-decoration-none">Back</a>
                    Create New Post
                    <button type="submit" class="btn" name="post-submit">Submit</button>
                </div>
                <div id="postP-preview">
                    <div class="card overflow-hidden">
                        <div class="row">
                            <div class="col p-0">
                                <!-- <img src="../assets/img/posts/ig.jpg" alt="" /> -->
                                <div style="text-align:center;
                                            padding:3%;">
                                    <div class="ratio ratio-1x1">
                                        <label for="inputTag" class="d-flex justify-content-center align-items-center"
                                            style="cursor: pointer;" onchange="preview()">
                                            <p id="inputText">Select Image</p> <br />
                                            <input id="inputTag" type="file" name="photo" style="display: none;" />
                                            <img id="thumb" src="">
                                            <br />
                                            <span id="imageName" style="color: green;"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col p-0 d-flex flex-column border-start">
                                <div class="card-header">
                                    <div class="d-flex flex-row align-items-center">
                                        <div
                                            class="rounded-circle overflow-hidden d-flex justify-content-center align-items-center border post-profile-photo me-3 ratio ratio-1x1">
                                            <img src="../assets/img/profiles/<?= $profileInfo['profile_photo'] ?>"
                                                alt="">
                                        </div>
                                        <span class="fw-bold flex-grow-1"><?= $_SESSION['user'] ?></span>
                                        <!-- dropdown delete -->

                                    </div>
                                </div>
                                <div class="card-body p-0 flex-grow-1">
                                    <!-- Post Body -->
                                    <div class="d-flex flex-column">
                                        <textarea name="caption" id="" cols="30" rows="26"
                                            class="border-0 shadow-none form-control focus"
                                            placeholder="Write a caption..."></textarea>
                                    </div>
                                    <!-- End Post Body -->
                                </div>


                                <!-- End Add Comment -->
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
    thumb.style.display = 'none';

    function preview() {
        thumb.style.display = 'block';

        thumb.src = URL.createObjectURL(event.target.files[0]);

        inputText.style.display = 'none';

    }
    </script>
</body>

</html>